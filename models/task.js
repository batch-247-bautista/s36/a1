const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
	default: "complete"
	}
});





module.exports = mongoose.model("Task", taskSchema);
// "module.exports" is a way for Node JS to treat this value as a package taht can be used by other files
