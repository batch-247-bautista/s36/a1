const express = require("express");
const router = express.Router();

const taskController = require("../controllers/taskController");

router.get("/", (req,res) => {

	// It invokes getAllTasks function from the taskController,js
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// [ SECTION ] Create a new task

router.post("/", (req,res) => {

	taskController.createTask(req.body).then(resultFromController => res.send(
				resultFromController));
})

// [ SECTION ]  Delete a Task

router.delete("/:id", (req,res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(
		resultFromController));
})


// [SECTION]  Update a task

router.put("/:id", (req,res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController =>
		res.send(resultFromController));
})



// Getting a specific task

router.get("/:id", (req,res) => {

	// It invokes getspecificTasks function from the taskController,js
	taskController.getSpecificTasks(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});


router.patch("/:id/complete", (req,res) => {

	// It invokes getspecificTasks function from the taskController,js
	taskController.getSpecificTasksComp(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});

module.exports = router;