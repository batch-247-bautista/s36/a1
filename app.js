// Set up our dependencies
const express = require("express");
const mongoose = require("mongoose");
const taskRoute = require("./routes/taskRoute");

// Server set-up

const app = express();
const port = 2001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// MongoDB Connection

mongoose.connect("mongodb+srv://admin:admin123@zuitt-bootcamp.xqf7owy.mongodb.net/s36-todo?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
}
);

mongoose.connection.once("open", () => console.log("Now connected to the database"));

// Add the task route
app.use("/tasks", taskRoute); // By writing this, all the task routes would start with /tasks


// Server listening

app.listen(port, () => console.log(`Now Listening to port ${port}`));