const Task = require("../models/task");

module.exports.getAllTasks = () => {
	return Task.find({}).then((result) => {
		return result;
	})
}

// The reuest body coming from the client was passed from the "taskRoute.js" file via "re.body" as an arguments and is renamed a "requestbody" parameter in the controller file

module.exports.createTask = (requestBody) => {
	// Creates a task object on the Mongoose model "task"
	let newTask = new Task({

		// Sets the "name" property with the value received from the client/postman
		name : requestBody.name
	})

	return newTask.save().then((task,error) => {
		if (error) {
			console.log(error);
			return false;
		}else {
			return task;
		}
	})
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask,err) => {
		if(err) {
			console.log(err);
			return false;
		} else {
			return removedTask;
		}
	})
}

module.exports.updateTask = (taskId, newContent) => {

	// The findByID, is a mongoose method that will look for a task with the same id provided from teh URL
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}

		// Results of the "findById" method will be stored in the result parameter
		result.name = newContent.name;

		return result.save().then((updateTask, saveErr) => {
			if (saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updateTask;
			}
		})
	})
}


module.exports.getSpecificTasks = () => {
	return Task.find({}).then((result) => {
		return result;
	})
}

module.exports.getSpecificTasksComp = () => {
	return Task.find({}).then((result) => {
		return result;
	})
}
